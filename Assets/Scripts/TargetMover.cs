using UnityEngine;
using Random = UnityEngine.Random;

public class TargetMover : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] Transform[] _waypoints;
    
    private int _currentIndexWaypoint;
    private float _distanceToPoint = 2;
    
    private void Update()
    {
        if (_waypoints.Length > 0)
        {
            if (Vector3.Distance(transform.position, _waypoints[_currentIndexWaypoint].position) <= _distanceToPoint || _currentIndexWaypoint < 0)
            {
                _currentIndexWaypoint = Random.Range(0, _waypoints.Length);
            }
            
            transform.position = Vector3.MoveTowards(transform.position, _waypoints[_currentIndexWaypoint].position,
                _speed * Time.deltaTime);
        }
    }
}
