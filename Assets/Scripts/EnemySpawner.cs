using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Transform[] _spawnPoints;
    [SerializeField] private Enemy _prefab;
    [SerializeField] private float _delay;
    [SerializeField] private Transform[] _targets;

    private void Start()
    {
        StartCoroutine(WaitingSpawn(_delay));
    }
    
    private IEnumerator WaitingSpawn(float delay)
    {
        var wait = new WaitForSeconds(delay);

        while (true)
        {
            SpawnEnemy();
            yield return wait;
        }
    }
    
    private void SpawnEnemy()
    {
        int spawnNumber = Random.Range(0, _spawnPoints.Length);
        Transform spawn = _spawnPoints[spawnNumber];
        
        int targetSpawnNumber = Random.Range(0, _targets.Length);
        Transform target = _targets[targetSpawnNumber];
        
        Enemy enemy = Instantiate(_prefab);
        enemy.transform.position = spawn.transform.position;
        enemy.GetComponent<EnemyMovment>().Init(target);
    }
}
