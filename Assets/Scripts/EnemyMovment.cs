using UnityEngine;

public class EnemyMovment : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private Transform _target;
    
    private void Update()
    {
        if (_target)
            transform.position = Vector3.Lerp(transform.position, _target.position, _speed * Time.deltaTime);
    }

    public void Init(Transform target)
    {
        _target = target;
    }
}
